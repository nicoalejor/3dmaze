#pragma once
#include <vector>
#include "raylib.h"
#include "GlobalGameDefines.h"

class IScreen {
public:
	virtual void Init(const std::vector<IScreen*>& _screensPtr) = 0;
	virtual IScreen* Update() = 0;
	virtual void Draw() = 0;
};