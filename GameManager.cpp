#include "GameManager.h"
#include "raymath.h"
#include <sstream>
#include <iostream>

void GameManager::Init() {

	mapsStruct map1;
	map1.mapNumber = 1;
	map1.initPlayerPos = { 5.0f, 3.0f };
	map1.winPlayerPos = { 17.0f, 7.0f };
	map1.timeToComplete = 300 * FPS;	//Time in Seconds * FPS
	map1.filename = "./resources/map1.png";
	map1.objectivesToComplete = 5;

	mapsDictionary.insert(std::make_pair(1, map1));

	mapsStruct map2;
	map2.mapNumber = 2;
	map2.initPlayerPos = { 7.0f, 17.0f };
	map2.winPlayerPos = { 1.0f, 3.0f };
	map2.timeToComplete = 300 * FPS;	//Time in Seconds * FPS
	map2.filename = "./resources/map2.png";
	map2.objectivesToComplete = 6;

	mapsDictionary.insert(std::make_pair(2, map2));
	
	mapsStruct map3;
	map3.mapNumber = 3;
	map3.initPlayerPos = { 15.0f, 15.0f };
	map3.winPlayerPos = { 11.0f, 5.0f };
	map3.timeToComplete = 300 * FPS;	//Time in Seconds * FPS
	map3.filename = "./resources/map3.png";
	map3.objectivesToComplete = 7;

	mapsDictionary.insert(std::make_pair(3, map3));

	currentTime = mapsDictionary[1].timeToComplete;
}

bool GameManager::CheckLose()
{	
	if (normalizedHP <= 0.0f) {
		gameoverText = "You had a horrible death trying to escape the complex\nLava was not able to support you anymore.";
		currentTime = mapsDictionary[1].timeToComplete;
		return true;
	}

	if (currentTime <= 0) {
		gameoverText = "You died, unable to escape the complex!\nNow they will continue to subject you to horrible experiments!";
		currentTime = mapsDictionary[1].timeToComplete;
		return true;
	}
	return false;	
}

bool GameManager::CheckWin(int mapNumber)
{	
	Vector2 winningPos = mapsDictionary[mapNumber].winPlayerPos;	
	if (CheckCollisionCircleRec(playerPos, 0.1f, { winningPos.x - 0.5f , winningPos.y - 0.5f, 1.0f, 1.0f })) {
		int objectToCompleteLvl = mapsDictionary[mapNumber].objectivesToComplete;
		if (acquiredMinerals >= objectToCompleteLvl) {
			dialog = "";
			return true;
		}
		else {
			dialog = "Need " + std::to_string(objectToCompleteLvl-acquiredMinerals) + " more mineral\s to activate portal";
		}
	}
			
	return false;		
}

void GameManager::SetTimer(int _time)
{
	currentTime = _time;
}

void GameManager::DrawUI()
{
	//HP text
	DrawText("HP", 40, 60, 40, GREEN);
	//Health Background
	DrawRectangle(100, 50, 200, 60, RED);
	//Health Bar
	DrawRectangle(100, 50, normalizedHP*200.0f, 60, GREEN);

	//Draw player dialog text
	DrawCenterText(dialog.c_str(), 700, 50, WHITE);

	//Draw timer
	int seconds = (currentTime / 60);
	int minutes = seconds / 60;
	seconds = seconds % 60;
	char buffer[20];
	sprintf_s(buffer, "Time %02d:%02d", minutes, seconds);
	DrawText(buffer, 40, 130, 40, WHITE);
}


void GameManager::UpdateGameManager(float _normalizedHP, std::string _dialog, Vector2 _playerPos, int _objAcquired) 
{
	normalizedHP = _normalizedHP;
	dialog = _dialog;
	playerPos = _playerPos;
	acquiredMinerals = _objAcquired;
	currentTime--;
}



void GameManager::DrawCenterText(const char* text, float positionY, int _fontSize, Color _color)
{
	std::stringstream ss(text);
	std::string to;
	float offsetY = 0;

	if (text != NULL)
	{
		while (std::getline(ss, to, '\n')) {
			int textWidth = MeasureText(to.c_str(), _fontSize);
			DrawText(to.c_str(), SCREEN_WIDTH / 2 - textWidth / 2, positionY + offsetY, _fontSize, _color);
			offsetY += 50;
		}
	}

}


