#pragma once
#include <stdlib.h>
#include "raylib.h"
#include <vector>

class MeshManager {

public:
	typedef struct modelStruct {
		Model* model = nullptr;
		Vector3 modelPosition;
		BoundingBox bBox;
		float scale;
		Color color = WHITE;
	} modelStruct;

	void Init();
	void DrawModels();
	void ClearCurrentModels();
	Mesh GenMeshCubicmapCustom(Image cubicmap, Vector3 cubeSize);
	void Unload();

	static MeshManager& instance()
	{
		static MeshManager* instance = new MeshManager();
		return *instance;
	}

	std::vector<modelStruct>& GetModelsInMap();

private:
	std::vector<modelStruct> modelsDrawVector;
	std::vector<Model> models;
	std::vector<float> modelsScale;
	std::vector<float> yPosVector;

	MeshManager() {}
};
