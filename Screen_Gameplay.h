#pragma once
#include "IScreen.h"
#include "GlobalGameDefines.h"
#include "TextureManager.h"
#include "Player.h"
#include "GameManager.h"

class Screen_Gameplay : public IScreen {

public:

	void Init(const std::vector<IScreen*>& _screensPtr) override;
	IScreen* Update() override;
	void Draw() override;
	void UnloadComponents();
	void LoadMapFromImage(const char* filename, Model& model);

private:	
	IScreen* endScreen = nullptr;	

	Player* player;
	Color* mapPixels;
	Texture2D cubicmap;
	Vector3 mapPosition = { 0,0,0 };
	Image imMap;	
	Model currentMapModel;		
	
	GameManager::mapsStruct currentMap;
	std::map<int, GameManager::mapsStruct>* mapsDictionary;
	
	int screenLoadingTime = 3 * FPS;
	int currentLoadingTime;
	bool isLoadingScreen = false;

	bool ChangeMap();
	void ResetGameplay();
	void UpdateLoading();
};