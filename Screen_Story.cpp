#include "Screen_Story.h"
#include "Screen_Title.h"
#include <sstream>

void Screen_Story::Init(const std::vector<IScreen*>& _screensPtr)
{
	for (auto& b : _screensPtr)
	{
		if (typeid(*b) == typeid(Screen_Title)) {
			titleScreen = b;
			break;
		}
	}
}

IScreen* Screen_Story::Update() {

	if (IsKeyPressed(KEY_O)) return titleScreen;	
	
	return this;	
}

void Screen_Story::Draw() {
	DrawCenterText("Story so far", SCREEN_HEIGHT / 8, titleFontSize);
	DrawCenterText("You are a superior lava being from outer lava dimension,\nin one of your expeditions arrived Earth, however\n you were careless and got captured by earthlings.\nEnclosed in a lab underground you were subject of awful experiments.\n\nInadvertedly luck came by your side, the underground lab was near a volcano,\nit started erupting from one moment to next.\n You knew that was your time to escape, but time is short and in this dimension you always\ntake damage when not on lava so be quick and make your escape!!!", SCREEN_HEIGHT / 4, titleFontSize);
	DrawCenterText("Press 'O' to return to Title.", SCREEN_HEIGHT / 4 + 500.0f, fontSize);
}

void Screen_Story::DrawCenterText(const char* text, float positionY, int _fontSize)
{
	std::stringstream ss(text);
	std::string to;
	float offsetY = 0;

	if (text != NULL)
	{
		while (std::getline(ss, to, '\n')) {
			int textWidth = MeasureText(to.c_str(), _fontSize);
			DrawText(to.c_str(), SCREEN_WIDTH / 2 - textWidth / 2, positionY + offsetY, _fontSize, WHITE);
			offsetY += 50;
		}
	}
}