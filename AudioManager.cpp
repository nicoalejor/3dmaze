#include "AudioManager.h"

Music themeMusic, gameplayMusic;
Sound hurtSFX;
static std::vector<Sound*> sfxVector;

void AudioManager::Init() {
	InitAudioDevice();

	//Initializing Music
	themeMusic = LoadMusicStream(themeMusicAddress);
	gameplayMusic = LoadMusicStream(gameplayMusicAddress);

	SetMusicVolume(themeMusic, 0.5f);
	SetMusicVolume(gameplayMusic, 0.3f);

	PlayMusicStream(themeMusic);

	//Initializing SFX
	hurtSFX = LoadSound(hurtSFXAddress);
	sfxVector.push_back(&hurtSFX);


	//SetSoundVolume(eatingPowerPillSFX, 0.25f);
}

//Checks current screen and plays music accordingly
void AudioManager::HandleMusic(const IScreen* currentScreen, const IScreen* gameplayScreen)
{
	if (currentScreen == gameplayScreen) {
		StopMusicStream(themeMusic);
		if (!IsMusicStreamPlaying(gameplayMusic)) {
			PlayMusicStream(gameplayMusic);
		}
		UpdateMusicStream(gameplayMusic);
	}
	else {
		StopMusicStream(gameplayMusic);
		if (!IsMusicStreamPlaying(themeMusic)) {
			PlayMusicStream(themeMusic);
		}
		UpdateMusicStream(themeMusic);		
	}
}

void AudioManager::PlayHurtSFX()
{
	if (!IsSoundPlaying(hurtSFX))
		PlaySound(hurtSFX);
}

void AudioManager::StopAllSFX()
{
	for (auto b : sfxVector) {
		if (IsSoundPlaying(*b)) StopSound(*b);
	}
}

void AudioManager::UnloadAudio()
{
	UnloadMusicStream(themeMusic);
	UnloadMusicStream(gameplayMusic);

	for (auto b : sfxVector) {
		UnloadSound(*b);		
	}

	CloseAudioDevice();
}


