#pragma once

#define SCREEN_WIDTH 1920	
#define SCREEN_HEIGHT 1080

#define FPS 60

#define RESOURCES_FOLDER "./resources/"

#define TILE_SIZE 32

#define SCREEN_CENTER {SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 }
