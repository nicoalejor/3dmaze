#pragma once
#include <string>
#include <map>
#include "TextureManager.h"
#include "GlobalGameDefines.h"

class GameManager {
public:
	typedef struct mapsStruct {
		int mapNumber;
		Vector2 initPlayerPos;
		Vector2 winPlayerPos;
		int timeToComplete;
		std::string filename;
		int objectivesToComplete;
	} mapsStruct;


	void SetGameoverText(std::string _text) { gameoverText = _text; }
	std::string GetGameoverText() { return gameoverText; }

	void Init();
	bool CheckLose();				//Check if the player lost	
	bool CheckWin(int mapNumber);				//Check if the player won
	void SetTimer(int _time);
	void UpdateGameManager(float _normalizedHP, std::string _dialog, Vector2 _playerPos, int _objAcquired);
	void DrawUI();

	static GameManager& instance()
	{
		static GameManager* instance = new GameManager();
		return *instance;
	}

	std::map<int, mapsStruct>* GetMaps() { return &mapsDictionary; }

private:
	float normalizedHP;
	float currentTime;
	Vector2 playerPos;
	int acquiredMinerals = 0;
	std::string dialog;
	std::string gameoverText;
	std::map<int, mapsStruct> mapsDictionary;

	void DrawCenterText(const char* text, float positionY, int _fontSize, Color _color);

	GameManager() {}	
};