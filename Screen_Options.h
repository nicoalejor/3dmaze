#pragma once
#include "IScreen.h"
#include "TextureManager.h"

class Screen_Options : public IScreen {
public:
	void Init(const std::vector<IScreen*>& _screensPtr) override;
	IScreen* Update() override;
	void Draw() override;

private:
	IScreen* titleScreen;
	float scale = 0.2f;
	unsigned char alpha = 255;
	Vector2 position = { (float)SCREEN_WIDTH / 2 - (float)TextureManager::GetLogo()->width / 2 * scale,
						 (float)SCREEN_HEIGHT / 2 - (float)TextureManager::GetLogo()->height / 2 * scale
	};

	int fontSize = 30;

	void DrawCenterText(const char* text, float positionY, int fontSize);
};
