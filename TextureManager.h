#pragma once
#include "raylib.h"
#include <vector>

class TextureManager {
public:
	void InitTextures();

	static const Texture2D* GetLogo();
	static const Texture2D* GetTitle();
	static const Texture2D* GetFullAtlas();

	void UnloadTextures();


private:
	
	const char* logoAddress = "./resources/menu/Logo.png";
	const char* titleAddress = "./resources/menu/introImg.png";
	const char* atlasAddress = "./resources/cubemap_atlas_full.png";
	
	std::vector<Texture2D*> texturesArray;

	
};