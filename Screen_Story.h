#pragma once
#include "IScreen.h"

class Screen_Story : public IScreen {
public:
	void Init(const std::vector<IScreen*>& _screensPtr) override;
	IScreen* Update() override;
	void Draw() override;

private:
	IScreen* titleScreen;

	int titleFontSize = 40;
	int fontSize = 30;

	void DrawCenterText(const char* text, float positionY, int fontSize);
}; 
