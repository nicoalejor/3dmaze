#include <iostream>
#include <sstream>
#include "Screen_Options.h"
#include "Screen_Title.h"

void Screen_Options::Init(const std::vector<IScreen*>& _screensPtr)
{
	for (auto& b : _screensPtr)
	{
		if (typeid(*b) == typeid(Screen_Title)) {
			titleScreen = b;
			break;
		}
	}
}

IScreen* Screen_Options::Update() {

	if (IsKeyPressed(KEY_O)) {
		return titleScreen;
	}
	else {
		return this;
	}
}

void Screen_Options::Draw() {
	DrawCenterText("Instructions", SCREEN_HEIGHT / 8, fontSize);
	DrawCenterText("To move use WASD and the mouse to rotate the camera.\nYou have an HP bar at the top left, when this bar is completely red you die.\nYou lose life everytime you are outside lava so be careful.\nYou also have limited time to complete each map,\nright below HP bar you have the time left.\nThere are 3 maps, to advance to the next map you need to activate a number of minerals hidden in the maze\nthen you should reach the teleportation tile (the one with the futuristic look).\nTo activate the minerals get close and press X.\n", SCREEN_HEIGHT / 8 + 100, fontSize);				
	DrawCenterText("Press 'O' to return to Title.", SCREEN_HEIGHT / 4 + 500.0f, fontSize);
}

void Screen_Options::DrawCenterText(const char* text, float positionY, int _fontSize)
{
	std::stringstream ss(text);
	std::string to;
	float offsetY = 0;

	if (text != NULL)
	{
		while (std::getline(ss, to, '\n')) {
			int textWidth = MeasureText(to.c_str(), _fontSize);
			DrawText(to.c_str(), SCREEN_WIDTH / 2 - textWidth / 2, positionY + offsetY, _fontSize, WHITE);
			offsetY += 50;
		}
	}

}