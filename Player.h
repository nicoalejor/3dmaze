#pragma once
#include "raylib.h"
#include "GlobalGameDefines.h"
#include "TextureManager.h"
#include "MeshManager.h"
#include <string>

class Player {
private:
	int maxHP = 100;
	int currentHP;
	bool notOnLava;
	int timerDmg = 1 * FPS;		//Player recieves dmg after 1 second outside lava.
	int currentTimerDmg;	//Current time outside lava, this resets when player gets back to lava.
	int dmgTaken = 1;		//Damage taken when not on lava.
	int objAcquired = 0;
	Vector3 oldPosition;
	Camera3D camera = { 0 };

	std::string dialogPlayer;

	void CheckObjectInRange();
	void CheckPlayerDmg();

public:
	Player(Vector2 _startPos);
	void UpdatePlayer(const Vector3& mapPosition, const Image& imMap, const Texture2D& cubicMap, const Color* mapPixels);
	void StartNewMap(Vector2 _startPos);

	//Getters and Setters
	Camera* GetCamera() { return &camera; }
	Vector2 GetPosition() { return { camera.position.x , camera.position.z }; }
	std::string GetDialogText() { return dialogPlayer; }
	float GetNormalizedHP() { return (float(currentHP) / maxHP); }
	int GetObjAcquired() { return objAcquired; }

	void PlayerDead(); //Called when player dies

};