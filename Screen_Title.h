#pragma once
#include "IScreen.h"
#include "TextureManager.h"

class Screen_Title : public IScreen {
public:
	void Init(const std::vector<IScreen*>& _screensPtr) override;
	IScreen* Update() override;
	void Draw() override;

private:
	IScreen* gameplayScreen;
	IScreen* optionScreen;
	IScreen* storyScreen;

	Vector2 positionTitle = { (float)SCREEN_WIDTH / 2 - (float)TextureManager::GetTitle()->width / 2,
								(float)SCREEN_HEIGHT / 4 - (float)TextureManager::GetTitle()->height / 2 };

	int fontSize = 30;

	void DrawCenterText(const char* text, float positionY, int fontSize);
};