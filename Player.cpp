#include "Player.h"
#include "AudioManager.h"
#include <iostream>

Player::Player(Vector2 _startPos)
{     
    currentHP = maxHP;

    camera.position = { _startPos.x, 0.5f, _startPos.y };  // Camera position
    camera.target = { 0.0f, 0.5f, 0.0f };      // Camera looking at point
    camera.up = { 0.0f, 1.0f, 0.0f };          // Camera up vector (rotation towards target)
    camera.fovy = 45.0f;                                // Camera field-of-view Y
    camera.projection = CAMERA_PERSPECTIVE;             // Camera mode type 

    SetCameraMode(camera, CAMERA_FIRST_PERSON);    
}

void Player::CheckObjectInRange()
{
    const Ray rayCast = GetMouseRay(SCREEN_CENTER, camera);
    std::vector<MeshManager::modelStruct>& modelsInMap = MeshManager::instance().GetModelsInMap();
    float maxDstRayHit = 1.0f;
    bool onObjectRange = false;
    bool isObjectActive = false;
    MeshManager::modelStruct* currentModelStruct = nullptr;

    for (auto& b : modelsInMap) {
        RayCollision boxHitInfo = GetRayCollisionBox(rayCast, b.bBox);
        if (boxHitInfo.hit && boxHitInfo.distance <= maxDstRayHit) {
            onObjectRange = true;

            if (b.color.r == RED.r && b.color.g == RED.g && b.color.b == RED.b) {
                isObjectActive = true;
            }
            else {
                dialogPlayer = "Mineral in range, press X";
                currentModelStruct = &b;
            }
            
        }
    }

    if (onObjectRange) {
        if (isObjectActive) {
            dialogPlayer = "Mineral already active";
        }
        else if (IsKeyPressed(KEY_X)) {
            currentModelStruct->color = RED;
            objAcquired++;
        }

    }
    else {
        dialogPlayer = "";
    }
}



void Player::UpdatePlayer(const Vector3& mapPosition, const Image& imMap, const Texture2D& cubicMap, const Color* mapPixels)
{
    oldPosition = camera.position;
    UpdateCamera(&camera);

    Vector2 playerPos = { camera.position.x, camera.position.z };
    float playerRadius = 0.1f;  // Collision radius (player is modelled as a cilinder for collision)

    int playerCellX = (int)(playerPos.x - mapPosition.x + 0.5f);
    int playerCellY = (int)(playerPos.y - mapPosition.z + 0.5f);

    // Out-of-limits security check
    if (playerCellX < 0) playerCellX = 0;
    else if (playerCellX >= imMap.width) playerCellX = imMap.width - 1;

    if (playerCellY < 0) playerCellY = 0;
    else if (playerCellY >= imMap.height) playerCellY = imMap.height - 1;

    notOnLava = false;
        
    for (int y = 0; y < cubicMap.height; y++)
    {
        for (int x = 0; x < cubicMap.width; x++)
        {
            // Check map collisions using image data and player position
            if (((mapPixels[y * cubicMap.width + x].r == 255) || (mapPixels[y * cubicMap.width + x].r == 128)) &&       // Collision: Check R channel
                (CheckCollisionCircleRec(playerPos, playerRadius, { mapPosition.x - 0.5f + x, mapPosition.z - 0.5f + y, 1.0f, 1.0f })))
            {
                // Collision detected, reset camera position
                camera.position = oldPosition;
            }
            
            //Check if player on lava 
            if ((mapPixels[y * cubicMap.width + x].b != 3 && mapPixels[y * cubicMap.width + x].b != 1) && (CheckCollisionCircleRec(playerPos, playerRadius, { mapPosition.x - 0.5f + x, mapPosition.z - 0.5f + y, 1.0f, 1.0f }))) {
                currentTimerDmg--;
                notOnLava = true;
            }         
        }
    }

    CheckObjectInRange();
    CheckPlayerDmg();
}

void Player::CheckPlayerDmg()
{
    if (!notOnLava) {
        currentTimerDmg = timerDmg;
    }

    if (currentTimerDmg <= 0) {
        currentTimerDmg = timerDmg;
        currentHP -= dmgTaken;
        AudioManager::PlayHurtSFX();
    }
}

//Handles player dead
void Player::PlayerDead()
{
	AudioManager::PlayHurtSFX();
}

void Player::StartNewMap(Vector2 _startPos)
{
    camera.position = { _startPos.x, 0.5f, _startPos.y };
    objAcquired = 0;
    currentHP = maxHP;
    currentTimerDmg = timerDmg;
}


