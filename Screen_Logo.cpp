#include <iostream>
#include "Screen_Logo.h"
#include "Screen_Title.h"

void Screen_Logo::Init(const std::vector<IScreen*>& _screensPtr)
{
	for (auto& b : _screensPtr)
	{
		if (typeid(*b) == typeid(Screen_Title)) {
			titleScreen = b;
			break;
		}					
	}
}

IScreen* Screen_Logo::Update() {

	if (timeLeftLogoScreen > 0) {
		timeLeftLogoScreen -= GetFrameTime();
		alpha = timeLeftLogoScreen * 255 / totalTimeLogoScreen;
		return this;
	}
	else {
		return titleScreen;
	}	
}

void Screen_Logo::Draw() {		
	DrawTextureEx(*TextureManager::GetLogo(), position, 0.0, scale, {255,255,255,alpha});
}