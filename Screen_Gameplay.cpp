#include <string>
#include <sstream>
#include "Screen_Gameplay.h"
#include "Screen_End.h"
#include "AudioManager.h"
#include "MeshManager.h"

void Screen_Gameplay::Init(const std::vector<IScreen*>& _screensPtr)
{
	for (auto& b : _screensPtr)
	{
		if (typeid(*b) == typeid(Screen_End)) {
			endScreen = b;
			break;
		}
	}

	currentLoadingTime = screenLoadingTime;

	GameManager::instance().Init();    

	mapsDictionary = GameManager::instance().GetMaps();
	currentMap = mapsDictionary->find(1)->second;
	LoadMapFromImage(currentMap.filename.c_str(), currentMapModel);
	player = new Player(currentMap.initPlayerPos);
}

IScreen* Screen_Gameplay::Update() {

	if (!isLoadingScreen) {
		player->UpdatePlayer(mapPosition, imMap, cubicmap, mapPixels);

		GameManager::instance().UpdateGameManager(player->GetNormalizedHP(), player->GetDialogText(), player->GetPosition(), player->GetObjAcquired());

		if (GameManager::instance().CheckLose()) {
			ResetGameplay();
			return endScreen;
		}
		else if (GameManager::instance().CheckWin(currentMap.mapNumber)) {
			if (ChangeMap()) {				
				ResetGameplay();
				GameManager::instance().SetGameoverText("You finally escaped the complex and now can return to your lava dimension!\n Hopefully your pursuers won't continue chasing your kind.");
				return endScreen;
			}
			else {
				player->StartNewMap(currentMap.initPlayerPos);

				isLoadingScreen = true;
			}
		}
	}
	else {
		UpdateLoading();
	}	

	return this;

}

void Screen_Gameplay::Draw() {
		 
	if (!isLoadingScreen) {
		BeginMode3D(*player->GetCamera());
		//Draw maze map
		DrawModel(currentMapModel, mapPosition, 1.0f, WHITE);
		MeshManager::instance().DrawModels();
		EndMode3D();

		GameManager::instance().DrawUI();
	}
	else {
		DrawText(("LOADING LEVEL " + std::to_string(currentMap.mapNumber)).c_str(), SCREEN_WIDTH / 2 - 250, SCREEN_HEIGHT / 2 - 10, 50, WHITE);
	}
	
}

void Screen_Gameplay::UnloadComponents()
{
    UnloadImage(imMap);             // Unload image from RAM
	UnloadImageColors(mapPixels);
    UnloadTexture(cubicmap);        // Unload cubicmap texture

	delete(player);
	UnloadModelKeepMeshes(currentMapModel);
}

void Screen_Gameplay::LoadMapFromImage(const char* filename, Model& model)
{		
	imMap = LoadImage(filename);      // Load cubicmap image (RAM)
	cubicmap = LoadTextureFromImage(imMap);       // Convert image to texture to display (VRAM)
	Mesh mesh = MeshManager::instance().GenMeshCubicmapCustom(imMap, { 1.0f, 1.0f, 1.0f });
	model = LoadModelFromMesh(mesh);
	model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = *TextureManager::GetFullAtlas(); // Set map diffuse texture

	// Get map image data to be used for collision detection
	mapPixels = LoadImageColors(imMap);	
}

bool Screen_Gameplay::ChangeMap()
{		
	if (currentMap.mapNumber == 3) return true;	//Win if last map reached	
		
	MeshManager::instance().ClearCurrentModels();

	int mapNumber = currentMap.mapNumber + 1;
	currentMap = mapsDictionary->find(mapNumber)->second;
	LoadMapFromImage(currentMap.filename.c_str(), currentMapModel);
	GameManager::instance().SetTimer(currentMap.timeToComplete);
	player->StartNewMap(currentMap.initPlayerPos);

	return false;
}

void Screen_Gameplay::ResetGameplay()
{
	MeshManager::instance().ClearCurrentModels();
	currentMap = mapsDictionary->find(1)->second;
	LoadMapFromImage(currentMap.filename.c_str(), currentMapModel);
	player->StartNewMap(currentMap.initPlayerPos);
}

void Screen_Gameplay::UpdateLoading()
{
	currentLoadingTime--;

	if (currentLoadingTime <= 0) {
		isLoadingScreen = false;
		currentLoadingTime = screenLoadingTime;
	}
}










