#include <vector>

#include "GlobalGameDefines.h"
#include "TextureManager.h"
#include "MeshManager.h"
#include "AudioManager.h"
#include "IScreen.h"
#include "Screen_Logo.h"
#include "Screen_Title.h"
#include "Screen_Story.h"
#include "Screen_Options.h"
#include "Screen_Gameplay.h"
#include "Screen_End.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------    
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "3D Maze, Floor is Lava");

    SetTargetFPS(FPS);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------
    
    //Initialize textures
    TextureManager textureManager;
    textureManager.InitTextures();    
    
    //Init MeshManager
    MeshManager::instance().Init();

    //Initialize sounds and music
    AudioManager audioManager;
    audioManager.Init();

    //Initialize the screens
    Screen_Logo screenLogo;
    Screen_Title screenTitle;
    Screen_Story screenStory;
    Screen_Options screenOptions;
    Screen_Gameplay screenGameplay;
    Screen_End screenEnd;

    IScreen* currentScreen = &screenLogo;
    
    const std::vector<IScreen*> screens = {&screenLogo, &screenTitle, &screenStory, &screenGameplay, &screenOptions, &screenEnd};

    for (auto& screen : screens) {
        screen->Init(screens);
    }

   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update        
        currentScreen = currentScreen->Update();
        
        AudioManager::HandleMusic(currentScreen, &screenGameplay);

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(BLACK);

        currentScreen->Draw();        

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------    
    textureManager.UnloadTextures();    //Unload all textures    
    audioManager.UnloadAudio();         //Unload all audio
    MeshManager::instance().Unload();   //Unload Models 
    screenGameplay.UnloadComponents();


    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}

