#pragma once
#include "raylib.h"
#include "IScreen.h"

class AudioManager {
public:

	void Init();

	static void HandleMusic(const IScreen* currentScreen, const IScreen* gameplayScreen);
	static void PlayHurtSFX();
	static void StopAllSFX();

	void UnloadAudio();

private:

	const char* themeMusicAddress = "./resources/music/intro.mp3";
	const char* gameplayMusicAddress = "./resources/music/gameplay.mp3";

	const char* hurtSFXAddress = "./resources/sfx/hurt.wav";
};