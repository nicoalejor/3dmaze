#include "MeshManager.h"
#include <raymath.h>

float sizeSide = 0.25f;
float spaceBetweenCubes = 0.5f;

std::vector<Rectangle> vecRightTexUV;
std::vector<Rectangle> vecLeftTexUV;
std::vector<Rectangle> vecFrontTexUV;
std::vector<Rectangle> vecBackTexUV;
std::vector<Rectangle> vecTopTexUV;
std::vector<Rectangle> vecBottomTexUV;

void MeshManager::Init()
{
    //Model loading      
        
    //Loading mineral
    models.push_back(LoadModel("resources/models/obj/rock5/Rock07-Base.obj"));
    Texture2D textureRock0 = LoadTexture("resources/models/obj/rock5/Texture/Rock07-Base-Diffuse.png"); // Load model texture
    models[models.size() - 1].materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = textureRock0;            // Set map diffuse texture 
    modelsScale.push_back(0.01f); 
    yPosVector.push_back(0.1f);

    //Loading mineral
    models.push_back(LoadModel("resources/models/obj/rock2/pebble.obj"));
    Texture2D textureRock1 = LoadTexture("resources/models/obj/rock2/Texture/pebble_Mat_Diffuse.png"); // Load model texture
    models[models.size() - 1].materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = textureRock1;            // Set map diffuse texture 
    modelsScale.push_back(0.025f);    
    yPosVector.push_back(0.0f);

    //Loading mineral
    models.push_back(LoadModel("resources/models/obj/rock1/CaveRock02_Obj.obj"));
    Texture2D textureRock2 = LoadTexture("resources/models/obj/rock1/Texture/CaveRock02_Base_Diffuse.png"); // Load model texture
    models[models.size() - 1].materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = textureRock2;            // Set map diffuse texture 
    modelsScale.push_back(0.015f);
    yPosVector.push_back(0.0f);

    //Loading mineral
    models.push_back(LoadModel("resources/models/obj/rock4/SpiderRock02_Obj.obj"));
    Texture2D textureRock3 = LoadTexture("resources/models/obj/rock4/Texture/SpiderRock02_Base-Diffuse.png"); // Load model texture
    models[models.size() - 1].materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = textureRock3;            // Set map diffuse texture 
    modelsScale.push_back(0.015f);
    yPosVector.push_back(0.2f);

    //Loading mineral
    models.push_back(LoadModel("resources/models/obj/rock3/rock_obj.obj"));
    Texture2D textureRock4 = LoadTexture("resources/models/obj/rock3/Texture/rocks_uw_BaseColor.png"); // Load model texture
    models[models.size() - 1].materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = textureRock4;            // Set map diffuse texture 
    modelsScale.push_back(0.20f);
    yPosVector.push_back(0.0f);

    //Init the UV vectors
    for (int y = 0; y < 2; y++)
    {
        for (int x = 0; x < 2; x++)
        {
            vecRightTexUV.push_back(Rectangle{ x * spaceBetweenCubes, y * spaceBetweenCubes, sizeSide, sizeSide });
            vecLeftTexUV.push_back(Rectangle{ sizeSide + (x * spaceBetweenCubes), y * spaceBetweenCubes, sizeSide, sizeSide });
            vecFrontTexUV.push_back(Rectangle{ x * spaceBetweenCubes, y * spaceBetweenCubes, sizeSide, sizeSide });
            vecBackTexUV.push_back(Rectangle{ sizeSide + (x * spaceBetweenCubes), y * spaceBetweenCubes, sizeSide, sizeSide });
            vecTopTexUV.push_back(Rectangle{ x * spaceBetweenCubes, sizeSide + (y * spaceBetweenCubes), sizeSide, sizeSide });
            vecBottomTexUV.push_back(Rectangle{ sizeSide + (x * spaceBetweenCubes), sizeSide + (y * spaceBetweenCubes), sizeSide, sizeSide });
        }
    }
}

void MeshManager::Unload()
{
    for (auto& b : modelsDrawVector)
    {
        b.model = nullptr;
    }

    for (auto& b : models)
    {
        UnloadModel(b);
    }
}

void MeshManager::DrawModels()
{
    for (int i = 0; i < modelsDrawVector.size(); i++)
    {
        DrawModel(*modelsDrawVector[i].model, modelsDrawVector[i].modelPosition, modelsDrawVector[i].scale, modelsDrawVector[i].color);
        //DrawBoundingBox(modelsDrawVector[i].bBox, GREEN);
    }  
}

void MeshManager::ClearCurrentModels()
{
    for (auto& b : modelsDrawVector)
    {
        b.model = nullptr;
    }
    modelsDrawVector.clear();
}


// Generate a cubes mesh from pixel data
// NOTE: Vertex data is uploaded to GPU
Mesh MeshManager::GenMeshCubicmapCustom(Image cubicmap, Vector3 cubeSize)
{
    Mesh mesh = { 0 };

    Color* pixels = LoadImageColors(cubicmap);

    int mapWidth = cubicmap.width;
    int mapHeight = cubicmap.height;

    // NOTE: Max possible number of triangles numCubes*(12 triangles by cube)
    int maxTriangles = cubicmap.width * cubicmap.height * 12;

    int vCounter = 0;       // Used to count vertices
    int tcCounter = 0;      // Used to count texcoords
    int nCounter = 0;       // Used to count normals

    float w = cubeSize.x;
    float h = cubeSize.z;
    float h2 = cubeSize.y;

    Vector3* mapVertices = (Vector3*)RL_MALLOC(maxTriangles * 3 * sizeof(Vector3));
    Vector2* mapTexcoords = (Vector2*)RL_MALLOC(maxTriangles * 3 * sizeof(Vector2));
    Vector3* mapNormals = (Vector3*)RL_MALLOC(maxTriangles * 3 * sizeof(Vector3));

    // Define the 6 normals of the cube, we will combine them accordingly later...
    Vector3 n1 = { 1.0f, 0.0f, 0.0f };
    Vector3 n2 = { -1.0f, 0.0f, 0.0f };
    Vector3 n3 = { 0.0f, 1.0f, 0.0f };
    Vector3 n4 = { 0.0f, -1.0f, 0.0f };
    Vector3 n5 = { 0.0f, 0.0f, -1.0f };
    Vector3 n6 = { 0.0f, 0.0f, 1.0f };

    //Set the UV for an atlas map with 4 cubes style, defined by default for the first cube style

    for (int z = 0; z < mapHeight; ++z)
    {
        for (int x = 0; x < mapWidth; ++x)
        {                        
            // Define the 8 vertex of the cube, we will combine them accordingly later...
            Vector3 v1 = { w * (x - 0.5f), h2, h * (z - 0.5f) };
            Vector3 v2 = { w * (x - 0.5f), h2, h * (z + 0.5f) };
            Vector3 v3 = { w * (x + 0.5f), h2, h * (z + 0.5f) };
            Vector3 v4 = { w * (x + 0.5f), h2, h * (z - 0.5f) };
            Vector3 v5 = { w * (x + 0.5f), 0, h * (z - 0.5f) };
            Vector3 v6 = { w * (x - 0.5f), 0, h * (z - 0.5f) };
            Vector3 v7 = { w * (x - 0.5f), 0, h * (z + 0.5f) };
            Vector3 v8 = { w * (x + 0.5f), 0, h * (z + 0.5f) };

            //Check the cube texture to be assigned
            int pixelB = pixels[z * cubicmap.width + x].b;
            
            Rectangle rightTexUV = vecRightTexUV[pixelB];
            Rectangle leftTexUV = vecLeftTexUV[pixelB];
            Rectangle frontTexUV = vecFrontTexUV[pixelB];
            Rectangle backTexUV = vecBackTexUV[pixelB];
            Rectangle topTexUV = vecTopTexUV[pixelB];
            Rectangle bottomTexUV = vecBottomTexUV[pixelB];

            // We check pixel color to be r = 255 -> draw full cube
            if(pixels[z * cubicmap.width + x].r == 255)
            {                                                
                // Define triangles and checking collateral cubes
                //------------------------------------------------                
                
                // Define top triangles (2 tris, 6 vertex --> v1-v2-v3, v1-v3-v4)
                // WARNING: Not required, created to allow seeing the map from outside
                mapVertices[vCounter] = v1;
                mapVertices[vCounter + 1] = v2;
                mapVertices[vCounter + 2] = v3;
                mapVertices[vCounter + 3] = v1;
                mapVertices[vCounter + 4] = v3;
                mapVertices[vCounter + 5] = v4;
                vCounter += 6;

                mapNormals[nCounter] = n3;
                mapNormals[nCounter + 1] = n3;
                mapNormals[nCounter + 2] = n3;
                mapNormals[nCounter + 3] = n3;
                mapNormals[nCounter + 4] = n3;
                mapNormals[nCounter + 5] = n3;
                nCounter += 6;

                mapTexcoords[tcCounter] = { topTexUV.x, topTexUV.y };
                mapTexcoords[tcCounter + 1] = { topTexUV.x, topTexUV.y + topTexUV.height };
                mapTexcoords[tcCounter + 2] = { topTexUV.x + topTexUV.width, topTexUV.y + topTexUV.height };
                mapTexcoords[tcCounter + 3] = { topTexUV.x, topTexUV.y };
                mapTexcoords[tcCounter + 4] = { topTexUV.x + topTexUV.width, topTexUV.y + topTexUV.height };
                mapTexcoords[tcCounter + 5] = { topTexUV.x + topTexUV.width, topTexUV.y };
                tcCounter += 6;

                // Define bottom triangles (2 tris, 6 vertex --> v6-v8-v7, v6-v5-v8)
                mapVertices[vCounter] = v6;
                mapVertices[vCounter + 1] = v8;
                mapVertices[vCounter + 2] = v7;
                mapVertices[vCounter + 3] = v6;
                mapVertices[vCounter + 4] = v5;
                mapVertices[vCounter + 5] = v8;
                vCounter += 6;

                mapNormals[nCounter] = n4;
                mapNormals[nCounter + 1] = n4;
                mapNormals[nCounter + 2] = n4;
                mapNormals[nCounter + 3] = n4;
                mapNormals[nCounter + 4] = n4;
                mapNormals[nCounter + 5] = n4;
                nCounter += 6;

                //Define which texture to use

                mapTexcoords[tcCounter] = { bottomTexUV.x + bottomTexUV.width, bottomTexUV.y };
                mapTexcoords[tcCounter + 1] = { bottomTexUV.x, bottomTexUV.y + bottomTexUV.height };
                mapTexcoords[tcCounter + 2] = { bottomTexUV.x + bottomTexUV.width, bottomTexUV.y + bottomTexUV.height };
                mapTexcoords[tcCounter + 3] = { bottomTexUV.x + bottomTexUV.width, bottomTexUV.y };
                mapTexcoords[tcCounter + 4] = { bottomTexUV.x, bottomTexUV.y };
                mapTexcoords[tcCounter + 5] = { bottomTexUV.x, bottomTexUV.y + bottomTexUV.height };
                tcCounter += 6;

                // Checking cube on bottom of current cube
                if (((z < cubicmap.height - 1) && (pixels[(z + 1) * cubicmap.width + x].r == 0 || pixels[(z + 1) * cubicmap.width + x].r == 128)) || (z == cubicmap.height - 1))
                {
                    // Define front triangles (2 tris, 6 vertex) --> v2 v7 v3, v3 v7 v8
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v2;
                    mapVertices[vCounter + 1] = v7;
                    mapVertices[vCounter + 2] = v3;
                    mapVertices[vCounter + 3] = v3;
                    mapVertices[vCounter + 4] = v7;
                    mapVertices[vCounter + 5] = v8;
                    vCounter += 6;

                    mapNormals[nCounter] = n6;
                    mapNormals[nCounter + 1] = n6;
                    mapNormals[nCounter + 2] = n6;
                    mapNormals[nCounter + 3] = n6;
                    mapNormals[nCounter + 4] = n6;
                    mapNormals[nCounter + 5] = n6;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { frontTexUV.x, frontTexUV.y };
                    mapTexcoords[tcCounter + 1] = { frontTexUV.x, frontTexUV.y + frontTexUV.height };
                    mapTexcoords[tcCounter + 2] = { frontTexUV.x + frontTexUV.width, frontTexUV.y };
                    mapTexcoords[tcCounter + 3] = { frontTexUV.x + frontTexUV.width, frontTexUV.y };
                    mapTexcoords[tcCounter + 4] = { frontTexUV.x, frontTexUV.y + frontTexUV.height };
                    mapTexcoords[tcCounter + 5] = { frontTexUV.x + frontTexUV.width, frontTexUV.y + frontTexUV.height };
                    tcCounter += 6;
                }

                // Checking cube on top of current cube
                if (((z > 0) && (pixels[(z - 1) * cubicmap.width + x].r == 0 || pixels[(z - 1) * cubicmap.width + x].r == 128)) || (z == 0))
                {
                    // Define back triangles (2 tris, 6 vertex) --> v1 v5 v6, v1 v4 v5
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v1;
                    mapVertices[vCounter + 1] = v5;
                    mapVertices[vCounter + 2] = v6;
                    mapVertices[vCounter + 3] = v1;
                    mapVertices[vCounter + 4] = v4;
                    mapVertices[vCounter + 5] = v5;
                    vCounter += 6;

                    mapNormals[nCounter] = n5;
                    mapNormals[nCounter + 1] = n5;
                    mapNormals[nCounter + 2] = n5;
                    mapNormals[nCounter + 3] = n5;
                    mapNormals[nCounter + 4] = n5;
                    mapNormals[nCounter + 5] = n5;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { backTexUV.x + backTexUV.width, backTexUV.y };
                    mapTexcoords[tcCounter + 1] = { backTexUV.x, backTexUV.y + backTexUV.height };
                    mapTexcoords[tcCounter + 2] = { backTexUV.x + backTexUV.width, backTexUV.y + backTexUV.height };
                    mapTexcoords[tcCounter + 3] = { backTexUV.x + backTexUV.width, backTexUV.y };
                    mapTexcoords[tcCounter + 4] = { backTexUV.x, backTexUV.y };
                    mapTexcoords[tcCounter + 5] = { backTexUV.x, backTexUV.y + backTexUV.height };
                    tcCounter += 6;
                }

                // Checking cube on right of current cube
                if (((x < cubicmap.width - 1) && (pixels[z * cubicmap.width + (x + 1)].r == 0 || pixels[z * cubicmap.width + (x+1)].r == 128)) || (x == cubicmap.width - 1))

                {
                    // Define right triangles (2 tris, 6 vertex) --> v3 v8 v4, v4 v8 v5
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v3;
                    mapVertices[vCounter + 1] = v8;
                    mapVertices[vCounter + 2] = v4;
                    mapVertices[vCounter + 3] = v4;
                    mapVertices[vCounter + 4] = v8;
                    mapVertices[vCounter + 5] = v5;
                    vCounter += 6;

                    mapNormals[nCounter] = n1;
                    mapNormals[nCounter + 1] = n1;
                    mapNormals[nCounter + 2] = n1;
                    mapNormals[nCounter + 3] = n1;
                    mapNormals[nCounter + 4] = n1;
                    mapNormals[nCounter + 5] = n1;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { rightTexUV.x, rightTexUV.y };
                    mapTexcoords[tcCounter + 1] = { rightTexUV.x, rightTexUV.y + rightTexUV.height };
                    mapTexcoords[tcCounter + 2] = { rightTexUV.x + rightTexUV.width, rightTexUV.y };
                    mapTexcoords[tcCounter + 3] = { rightTexUV.x + rightTexUV.width, rightTexUV.y };
                    mapTexcoords[tcCounter + 4] = { rightTexUV.x, rightTexUV.y + rightTexUV.height };
                    mapTexcoords[tcCounter + 5] = { rightTexUV.x + rightTexUV.width, rightTexUV.y + rightTexUV.height };
                    tcCounter += 6;
                }

                // Checking cube on left of current cube
                if (((x > 0) && (pixels[z * cubicmap.width + (x - 1)].r == 0 || pixels[z * cubicmap.width + (x-1)].r == 128)) || (x == 0))
                {
                    // Define left triangles (2 tris, 6 vertex) --> v1 v7 v2, v1 v6 v7
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v1;
                    mapVertices[vCounter + 1] = v7;
                    mapVertices[vCounter + 2] = v2;
                    mapVertices[vCounter + 3] = v1;
                    mapVertices[vCounter + 4] = v6;
                    mapVertices[vCounter + 5] = v7;
                    vCounter += 6;

                    mapNormals[nCounter] = n2;
                    mapNormals[nCounter + 1] = n2;
                    mapNormals[nCounter + 2] = n2;
                    mapNormals[nCounter + 3] = n2;
                    mapNormals[nCounter + 4] = n2;
                    mapNormals[nCounter + 5] = n2;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { leftTexUV.x, leftTexUV.y };
                    mapTexcoords[tcCounter + 1] = { leftTexUV.x + leftTexUV.width, leftTexUV.y + leftTexUV.height };
                    mapTexcoords[tcCounter + 2] = { leftTexUV.x + leftTexUV.width, leftTexUV.y };
                    mapTexcoords[tcCounter + 3] = { leftTexUV.x, leftTexUV.y };
                    mapTexcoords[tcCounter + 4] = { leftTexUV.x, leftTexUV.y + leftTexUV.height };
                    mapTexcoords[tcCounter + 5] = { leftTexUV.x + leftTexUV.width, leftTexUV.y + leftTexUV.height };
                    tcCounter += 6;
                }
            }
            // We check pixel color R to be 0 or 128, we will only draw floor and roof
            else if (pixels[z * cubicmap.width + x].r == 0 || pixels[z * cubicmap.width + x].r == 128)
            {
                //Check for 128 on red to set the green to some Model
                if (pixels[z * cubicmap.width + x].r == 128) {

                    unsigned char pixelG = pixels[z * cubicmap.width + x].g;

                    //If green pixel is different than zero we create a modelStruct and add it to the models
                    if (pixelG != 0) {
                        modelStruct modelAdd;
                        modelAdd.model = &models[pixelG - 1];
                        modelAdd.scale = modelsScale[pixelG - 1];
                        modelAdd.modelPosition = { static_cast<float>(x),yPosVector[pixelG - 1], static_cast<float>(z) };
                        BoundingBox boundingBox = GetMeshBoundingBox(modelAdd.model->meshes[0]);
                        boundingBox.min = Vector3Scale(boundingBox.min, modelAdd.scale);
                        boundingBox.min.x += x;
                        boundingBox.min.y += yPosVector[pixelG - 1];
                        boundingBox.min.z += z;
                        boundingBox.max = Vector3Scale(boundingBox.max, modelAdd.scale);
                        boundingBox.max.x += x;
                        boundingBox.max.y += yPosVector[pixelG - 1];
                        boundingBox.max.z += z;
                        modelAdd.bBox = boundingBox;

                        modelsDrawVector.push_back(modelAdd);

                    }
                }
                
                // Define top triangles (2 tris, 6 vertex --> v1-v2-v3, v1-v3-v4)
                mapVertices[vCounter] = v1;
                mapVertices[vCounter + 1] = v3;
                mapVertices[vCounter + 2] = v2;
                mapVertices[vCounter + 3] = v1;
                mapVertices[vCounter + 4] = v4;
                mapVertices[vCounter + 5] = v3;
                vCounter += 6;

                mapNormals[nCounter] = n4;
                mapNormals[nCounter + 1] = n4;
                mapNormals[nCounter + 2] = n4;
                mapNormals[nCounter + 3] = n4;
                mapNormals[nCounter + 4] = n4;
                mapNormals[nCounter + 5] = n4;
                nCounter += 6;

                mapTexcoords[tcCounter] = { topTexUV.x, topTexUV.y };
                mapTexcoords[tcCounter + 1] = { topTexUV.x + topTexUV.width, topTexUV.y + topTexUV.height };
                mapTexcoords[tcCounter + 2] = { topTexUV.x, topTexUV.y + topTexUV.height };
                mapTexcoords[tcCounter + 3] = { topTexUV.x, topTexUV.y };
                mapTexcoords[tcCounter + 4] = { topTexUV.x + topTexUV.width, topTexUV.y };
                mapTexcoords[tcCounter + 5] = { topTexUV.x + topTexUV.width, topTexUV.y + topTexUV.height };
                tcCounter += 6;

                // Define bottom triangles (2 tris, 6 vertex --> v6-v8-v7, v6-v5-v8)
                mapVertices[vCounter] = v6;
                mapVertices[vCounter + 1] = v7;
                mapVertices[vCounter + 2] = v8;
                mapVertices[vCounter + 3] = v6;
                mapVertices[vCounter + 4] = v8;
                mapVertices[vCounter + 5] = v5;
                vCounter += 6;

                mapNormals[nCounter] = n3;
                mapNormals[nCounter + 1] = n3;
                mapNormals[nCounter + 2] = n3;
                mapNormals[nCounter + 3] = n3;
                mapNormals[nCounter + 4] = n3;
                mapNormals[nCounter + 5] = n3;
                nCounter += 6;

                mapTexcoords[tcCounter] = { bottomTexUV.x + bottomTexUV.width, bottomTexUV.y };
                mapTexcoords[tcCounter + 1] = { bottomTexUV.x + bottomTexUV.width, bottomTexUV.y + bottomTexUV.height };
                mapTexcoords[tcCounter + 2] = { bottomTexUV.x, bottomTexUV.y + bottomTexUV.height };
                mapTexcoords[tcCounter + 3] = { bottomTexUV.x + bottomTexUV.width, bottomTexUV.y };
                mapTexcoords[tcCounter + 4] = { bottomTexUV.x, bottomTexUV.y + bottomTexUV.height };
                mapTexcoords[tcCounter + 5] = { bottomTexUV.x, bottomTexUV.y };
                tcCounter += 6;
            }
        }
    }

    // Move data from mapVertices temp arays to vertices float array
    mesh.vertexCount = vCounter;
    mesh.triangleCount = vCounter / 3;

    mesh.vertices = (float*)RL_MALLOC(mesh.vertexCount * 3 * sizeof(float));
    mesh.normals = (float*)RL_MALLOC(mesh.vertexCount * 3 * sizeof(float));
    mesh.texcoords = (float*)RL_MALLOC(mesh.vertexCount * 2 * sizeof(float));
    mesh.colors = NULL;

    int fCounter = 0;

    // Move vertices data
    for (int i = 0; i < vCounter; i++)
    {
        mesh.vertices[fCounter] = mapVertices[i].x;
        mesh.vertices[fCounter + 1] = mapVertices[i].y;
        mesh.vertices[fCounter + 2] = mapVertices[i].z;
        fCounter += 3;
    }

    fCounter = 0;

    // Move normals data
    for (int i = 0; i < nCounter; i++)
    {
        mesh.normals[fCounter] = mapNormals[i].x;
        mesh.normals[fCounter + 1] = mapNormals[i].y;
        mesh.normals[fCounter + 2] = mapNormals[i].z;
        fCounter += 3;
    }

    fCounter = 0;

    // Move texcoords data
    for (int i = 0; i < tcCounter; i++)
    {
        mesh.texcoords[fCounter] = mapTexcoords[i].x;
        mesh.texcoords[fCounter + 1] = mapTexcoords[i].y;
        fCounter += 2;
    }

    RL_FREE(mapVertices);
    RL_FREE(mapNormals);
    RL_FREE(mapTexcoords);

    UnloadImageColors(pixels);   // Unload pixels color data

    // Upload vertex data to GPU (static mesh)
    UploadMesh(&mesh, false);

    return mesh;
}

std::vector<MeshManager::modelStruct>& MeshManager::GetModelsInMap()
{
    return modelsDrawVector;
}


