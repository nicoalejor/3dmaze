#pragma once
#include "IScreen.h"

class Screen_End : public IScreen {
public:
	void Init(const std::vector<IScreen*>& _screensPtr) override;
	IScreen* Update() override;
	void Draw() override;

private:
	IScreen* optionScreen;
	IScreen* gameplayScreen;

	Vector2 positionInitText; 

	int fontSize = 30;

	void DrawCenterText(const char* text, float positionY, int fontSize);
};

