#include <iostream>
#include <string>
#include <sstream>

#include "Screen_Title.h"
#include "Screen_Gameplay.h"
#include "Screen_Story.h"
#include "Screen_Options.h"
#include "AudioManager.h"


void Screen_Title::Init(const std::vector<IScreen*>& _screensPtr)
{
	for (auto& b : _screensPtr)
	{
		if (typeid(*b) == typeid(Screen_Gameplay)) {			
			gameplayScreen = b;
		}
		else if (typeid(*b) == typeid(Screen_Options)) {
			optionScreen = b;
		}
		else if (typeid(*b) == typeid(Screen_Story)) {
			storyScreen = b;
		}
	}
}

IScreen* Screen_Title::Update()
{
	if (IsKeyPressed(KEY_ENTER)) {
		return gameplayScreen;
	}
	else if (IsKeyPressed(KEY_O)) {
		return optionScreen;
	}
	else if (IsKeyPressed(KEY_S)) {
		return storyScreen;
	}
	
	return this;	
}

void Screen_Title::Draw()
{
	DrawTextureV(*TextureManager::GetTitle(), positionTitle, WHITE);
	DrawCenterText("Escape from the Inferno Lab by Nicolas R.", positionTitle.y + 1050.0f, 40);
	DrawCenterText("Press Enter for Playing.\nPress 'S' for Story.\nPress 'O' for Instructions.", positionTitle.y + 1150.0f, fontSize);
}

void Screen_Title::DrawCenterText(const char* text, float positionY, int _fontSize)
{
	std::stringstream ss(text);
	std::string to;
	float offsetY = 0;

	if (text != NULL)
	{
		while (std::getline(ss, to, '\n')) {
			int textWidth = MeasureText(to.c_str(), _fontSize);
			DrawText(to.c_str(), SCREEN_WIDTH / 2 - textWidth / 2, positionY + offsetY, _fontSize, WHITE);
			offsetY += 50;
		}
	}
}
