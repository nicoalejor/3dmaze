#include "TextureManager.h"

static Texture2D logo, title, fullAtlas;

void TextureManager::InitTextures()
{

	logo = LoadTexture(logoAddress);	
	texturesArray.push_back(&logo);

	title = LoadTexture(titleAddress);
	texturesArray.push_back(&title);
	
	// Load atlas texture
	fullAtlas = LoadTexture(atlasAddress);
	texturesArray.push_back(&fullAtlas);

}

const Texture2D* TextureManager::GetLogo()
{
	return &logo; 
}

const Texture2D* TextureManager::GetTitle()
{
	return &title;
}

const Texture2D* TextureManager::GetFullAtlas()
{
	return &fullAtlas;
}



void TextureManager::UnloadTextures()
{
	for (int i = 0; i < texturesArray.size(); i++)
	{
		UnloadTexture(*texturesArray[i]);
		
	}	
}

