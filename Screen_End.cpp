#include <iostream>
#include <sstream>
#include "GameManager.h"
#include "Screen_End.h"
#include "Screen_Gameplay.h"
#include "Screen_Options.h"
#include "AudioManager.h"

void Screen_End::Init(const std::vector<IScreen*>& _screensPtr)
{
	for (auto& b : _screensPtr)
	{
		if (typeid(*b) == typeid(Screen_Gameplay)) {
			gameplayScreen = b;
		}
		else if (typeid(*b) == typeid(Screen_Options)) {
			optionScreen = b;
		}
	}
}

IScreen* Screen_End::Update()
{
	if (IsKeyPressed(KEY_ENTER)) {
		return gameplayScreen;
	}
	else if (IsKeyPressed(KEY_O)) {
		return optionScreen;
	}
	else
	{
		return this;
	}
}

void Screen_End::Draw()
{	
	std::string textGameover = GameManager::instance().GetGameoverText();
	DrawCenterText(textGameover.c_str(), SCREEN_HEIGHT / 2, fontSize);
	DrawCenterText("Press Enter to Play Again \n Press 'O' for Instructions", SCREEN_HEIGHT / 2 + 200.0f, fontSize);	
}

void Screen_End::DrawCenterText(const char* text, float positionY, int _fontSize)
{
	std::stringstream ss(text);
	std::string to;
	float offsetY = 0;

	if (text != NULL)
	{
		while (std::getline(ss, to, '\n')) {
			int textWidth = MeasureText(to.c_str(), _fontSize);
			DrawText(to.c_str(), SCREEN_WIDTH / 2 - textWidth / 2, positionY + offsetY, _fontSize, WHITE);
			offsetY += 50;
		}
	}
}